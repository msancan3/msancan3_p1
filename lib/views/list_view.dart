import 'package:flutter/material.dart';
import 'package:msancan3_p1/views/product_details_view.dart';
import 'package:msancan3_p1/models/product_model.dart';
import 'dart:convert';

class WaitIndicator extends StatelessWidget {
  const WaitIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //Widget del Indicador de carga
          Container(
            width: 200,
            height: 200,
            child: const CircularProgressIndicator(
              color: Colors.black,
            ),
          ),

          //Separador entre el texto y el Widget de carga
          const SizedBox(
            height: 20,
          ),

          //Texto que indica el estado de carga
          const Text(
            "Loading",
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}

class ProductsListView extends StatelessWidget {
  const ProductsListView({super.key});

  Future<Products> getItems(BuildContext context) async {
    //Cargamos el json
    var datos =
        await DefaultAssetBundle.of(context).loadString('assets/products.json');
    //Decodificamos sus datos
    Map<String, dynamic> dict = json.decode(datos);
    //Devolvemos los datos decodificados
    var items = Products.fromJson(dict);
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: const IconThemeData(color: Colors.white),
          title: const Text(
            'Products List',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.black,
        ),
        body: FutureBuilder(
          //El constructor de futuro
          future: getItems(context), //pedimos la información
          builder:
              (BuildContext context, AsyncSnapshot<Products> asyncSnapshot) {
            if (!asyncSnapshot.hasData) {
              //Si no hay datos cargados aún mostramos un indicador de carga
              return const WaitIndicator(); //es una clase
            } else {
              //Me guardo en una variable los datos extraídos del json
              final items = asyncSnapshot.data!.products;
              return ListView.separated(
                  itemBuilder: (BuildContext ctx, index) {
                    String rutaImagen = "";
                    //Extraemos de la variable items la ruta de la imagen
                    rutaImagen = items[index].image;
                    //Devolvemos una lista
                    return ListTile(
                      //La imagen del producto con la animación hero
                      leading: Hero(
                          tag: items[index].name,
                          child: Image.asset(
                            rutaImagen,
                          )),
                      //el nombre del producto
                      title: Text(
                        items[index].name,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 13),
                      ),
                      //el precio del producto
                      subtitle: Text("${items[index].price} €"),
                      //la flecha
                      trailing: const Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.grey,
                      ),

                      //Cambiar de página
                      onTap: () {
                        _goToNextPage(context, items, index);
                      },
                    );
                  },
                  separatorBuilder: (BuildContext ctx, index) {
                    return const Divider(
                      thickness: 4,
                    );
                  },
                  itemCount: items.length);
            }
          },
        ));
  }

  void _goToNextPage(BuildContext context, var items, int index) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductDetails(
                  item: items[index],
                )));
  }
}
