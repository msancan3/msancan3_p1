import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:msancan3_p1/models/product_model.dart';

class ProductDetails extends StatelessWidget {
  const ProductDetails({super.key, required this.item});
  final Product item;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.white),
        title: const Text(
          'Product Details',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.black,
      ),
      body: details(context),
      backgroundColor: Colors.grey,
    );
  }

  Widget image(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25.0),
          bottomRight: Radius.circular(25.0),
          topLeft: Radius.circular(25.0),
          topRight: Radius.circular(25.0),
        ),
      ),

      alignment: Alignment.center,
      height: MediaQuery.of(context).size.height /
          3, //Altura del contenedor que almacena la imagen
      width: MediaQuery.of(context).size.width * 4 / 5,

      child: ClipRRect(
        //la imagen
        borderRadius: BorderRadius.circular(30.0),
        child: Hero(
            tag: item.name,
            child: Image.asset(
              item.image,
              fit: BoxFit.scaleDown,
            )),
      ),
    );
  }

  Widget text(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
      ),
      child: Container(
        height: MediaQuery.of(context).size.height / 2 - 40,
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              style: const TextStyle(
                fontSize: 30.0,
              ),
              item.name,
            ),
            const SizedBox(height: 10.0),
            Text(
              style: const TextStyle(fontSize: 30.0),
              '${item.price} €',
            ),
            const SizedBox(height: 10.0),
            Text(
              style: const TextStyle(fontSize: 20.0),
              item.description,
            ),
          ],
        ),
      ),
    );
  }

  Widget details(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        image(context),
        const SizedBox(
          height: 40,
        ), //imagen
        text(context),
      ],
    );
  }
}
