import 'package:flutter/material.dart';
import 'package:msancan3_p1/views/list_view.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        //margin: const EdgeInsets.all(30),
        child: Column(
          children: [
            const SizedBox(width: 30, height: 50),
            _Header(),
            const SizedBox(width: 30, height: 50),
            _Body(),
            const SizedBox(width: 30, height: 50),
            _Footer(),
            const SizedBox(width: 30, height: 50),
          ],
        ),
      ),
    );
  }
}

class _Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          'Login',
          style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
        ),
        const SizedBox(height: 100, width: 70),
        _Circulos()
      ],
    );
  }
}

class _Circulos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.black,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.shade600,
                    blurRadius: 10,
                    spreadRadius: 1)
              ]),
        ),
        const SizedBox(height: 10, width: 10),
        Container(
          width: 95,
          height: 95,
          decoration: BoxDecoration(
              color: Colors.black,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.shade600,
                    blurRadius: 10,
                    spreadRadius: 1)
              ]),
        ),
        Container(
          width: 20,
          height: 20,
          decoration: BoxDecoration(
              color: Colors.black,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.shade600,
                    blurRadius: 10,
                    spreadRadius: 1)
              ]),
        ),
      ],
    );
  }
}

class _Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 300,
        height: 200,
        padding: const EdgeInsets.only(left: 20, right: 20, top: 60),
        decoration: BoxDecoration(
            color: const Color.fromARGB(255, 255, 255, 255),
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.shade600, blurRadius: 10, spreadRadius: 1)
            ]),
        child: Column(
          children: [
            _Username(),
            _Password(),
          ],
        ));
  }
}

class _Username extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Row(
          children: [
            SizedBox(width: 10, height: 5),
            Icon(
              Icons.person,
              color: Colors.black,
              size: 25,
            ),
            SizedBox(width: 10, height: 5),
            Text("Username")
          ],
        ),
        Container(
          height: 25,
          child: const TextField(
              decoration: InputDecoration(border: UnderlineInputBorder())),
        ),
      ],
    );
  }
}

class _Password extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Row(
          children: [
            SizedBox(width: 10, height: 5),
            Icon(
              Icons.vpn_key,
              color: Colors.black,
              size: 25,
            ),
            SizedBox(width: 10, height: 5),
            Text("Password")
          ],
        ),
        Container(
          height: 25,
          child: const TextField(
            decoration: InputDecoration(border: UnderlineInputBorder()),
            obscureText: true,
          ),
        ),
      ],
    );
  }
}

class _Footer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: _Button(),
      alignment: Alignment.centerRight,
    );
  }
}

class _Button extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _goToSecondPage(context),
      child: Container(
        width: 150,
        height: 40,
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              bottomLeft: Radius.circular(30),
            ),
            color: Colors.black),
        child: const Center(
          child: Text(
            'LOGIN',
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }

  void _goToSecondPage(BuildContext context) {
    var route = MaterialPageRoute(
      builder: (context) => const ProductsListView(),
    );
    Navigator.of(context).push(route);
  }
}
