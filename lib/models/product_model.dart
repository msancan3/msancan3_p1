class Products {
  late List<Product> products;

  Products({required this.products});

  Products.fromJson(Map<String, dynamic> json) {
    if (json['products'] != null) {
      products = <Product>[];
      json['products'].forEach((v) {
        products.add(Product.fromJson(v));
      });
    }
  }
}

class Product {
  late String name;
  late double price;
  late String image;
  late String description;

  Product({
    required this.name,
    required this.price,
    required this.image,
    required this.description,
  });

  Product.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    price = json['price'];
    image = json['image'];
    description = json['description'];
  }
}

/*
enum PhoneType { iphone, samsung, LG }

class Phone {
  String name;
  int number;
  PhoneType type;
  int price;

  Phone({
    required this.name,
    required this.number,
    required this.type,
    required this.price,
  });

  static final List<Phone> models = [
    Phone(
      name: 'Iphone 1',
      number: 1,
      type: PhoneType.iphone,
      price: 10,
    ),
    Phone(
      name: 'Samsung 10',
      number: 2,
      type: PhoneType.samsung,
      price: 1000,
    ),
    Phone(
      name: 'Iphone 24',
      number: 3,
      type: PhoneType.iphone,
      price: 5500,
    ),
    Phone(
      name: 'Samsung 8',
      number: 4,
      type: PhoneType.samsung,
      price: 500,
    ),
    Phone(
      name: 'LG 3',
      number: 5,
      type: PhoneType.LG,
      price: 300,
    ),
    Phone(
      name: 'Iphone 100',
      number: 6,
      type: PhoneType.iphone,
      price: 400,
    ),
    Phone(
      name: 'Samsung 15',
      number: 7,
      type: PhoneType.samsung,
      price: 7000,
    ),
    Phone(
      name: 'LG 13',
      number: 8,
      type: PhoneType.LG,
      price: 1700,
    ),
    Phone(
      name: 'Iphone 12',
      number: 9,
      type: PhoneType.iphone,
      price: 1600,
    ),
    Phone(
      name: 'Samsung 1',
      number: 10,
      type: PhoneType.samsung,
      price: 10,
    ),
    Phone(
      name: 'LG 10',
      number: 11,
      type: PhoneType.LG,
      price: 1000,
    ),
    Phone(
      name: 'LG 24',
      number: 12,
      type: PhoneType.LG,
      price: 5500,
    ),
    Phone(
      name: 'Samsung 8',
      number: 13,
      type: PhoneType.samsung,
      price: 500,
    ),
    Phone(
      name: 'Samsung 3',
      number: 14,
      type: PhoneType.samsung,
      price: 300,
    ),
    Phone(
      name: 'Samsung 100',
      number: 15,
      type: PhoneType.samsung,
      price: 400,
    ),
    Phone(
      name: 'Samsung 15',
      number: 16,
      type: PhoneType.samsung,
      price: 7000,
    ),
    Phone(
      name: 'Iphone 13',
      number: 17,
      type: PhoneType.iphone,
      price: 1700,
    ),
    Phone(
      name: 'Iphone 12',
      number: 18,
      type: PhoneType.iphone,
      price: 1600,
    ),
  ];
}
*/